package com.company.graph2;

import java.util.HashMap;
import java.util.*;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        HashMap<Integer, Integer> lengthsOfShortestPaths = new HashMap<>();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            lengthsOfShortestPaths.put(i, Integer.MAX_VALUE);
        }
        lengthsOfShortestPaths.put(startIndex, 0);
        Set<Integer> visitedVertices = new HashSet<>();
        visitedVertices.add(startIndex);
        while (visitedVertices.size() < adjacencyMatrix.length) {
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (i != startIndex && !visitedVertices.contains(i) && adjacencyMatrix[startIndex][i] > 0) {
                    int currentShortestLength = lengthsOfShortestPaths.get(i);
                    int shortestLengthCandidate = lengthsOfShortestPaths.get(startIndex) + adjacencyMatrix[startIndex][i];
                    if (shortestLengthCandidate < currentShortestLength) {
                        currentShortestLength = shortestLengthCandidate;
                        lengthsOfShortestPaths.put(i, currentShortestLength);
                    }
                }
            }
            int minLength = Integer.MAX_VALUE;
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (i != startIndex && !visitedVertices.contains(i) && lengthsOfShortestPaths.get(i) < minLength) {
                    startIndex = i;
                    minLength = lengthsOfShortestPaths.get(i);
                }
            }
            visitedVertices.add(startIndex);
        }
        return lengthsOfShortestPaths;
    }



    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        Set<Integer> setList = new TreeSet<>();
        int lengthsOfShortestTree = 0;
        int a = adjacencyMatrix.length;
        int startIndex = 0;
        setList.add(startIndex);
        while (setList.size() != a) {
            int u = 0;
            int v = 0;
            int minLength = Integer.MAX_VALUE;

            for (Integer i : setList) {
                for (int j = 0; j < adjacencyMatrix[i].length; j++) {
                    int w = adjacencyMatrix[i][j];
                    if (w < minLength && w != 0 && (!setList.contains(j))) {
                        u = i;
                        v = j;
                        minLength = w;
                    }
                }
            }
            setList.add(u);
            setList.add(v);
            lengthsOfShortestTree += minLength;
        }

        return lengthsOfShortestTree;
    }


    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        return null;
    }
}
